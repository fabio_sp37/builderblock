export const getUfsFetch = () => {
  return fetch('https://servicodados.ibge.gov.br/api/v1/localidades/estados', {
    method: 'GET',
  })
}

export const getMunicipiosFetch = uf => {
  return fetch(`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${uf}/municipios`, {
    method: 'GET',
  })
}