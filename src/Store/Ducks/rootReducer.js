import { combineReducers } from 'redux'
import casamentoReducer from './cadCasamentoReducer'
import ufsReducer from './getUfsReducer'
import municipiosReducer from './getMunicipiosReducer'

export default combineReducers({
  casamentoReducer,
  ufsReducer,
  municipiosReducer
})