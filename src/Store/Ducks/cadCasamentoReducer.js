import { createActions, createReducer } from 'reduxsauce';

//Actions types and creators
export const { Types, Creators } = createActions({
  postCasamentoSuccess: ["confirm", "err", "loading"],
  postCasamentoFailure: ["confirm", "err", "loading"],
  postCasamentoRequest: ["loading"]
})

//Reducer handlers
const INITIAL_STATE = {
  err: false,
  loading: false,
  confirm: false
}

const casamentoOk = (state = INITIAL_STATE, action) => (
  {
    confirm: action.confirm,
    err: false,
    loading: false
  }
)

const casamentoFail = (state = INITIAL_STATE, action) => (
  {
    confirm: false,
    err: action.err,
    loading: false
  }
)

const casamentoReq = (state = INITIAL_STATE, action) => (
  {
    confirm: false,
    err: false,
    loading: true
  }
)

//Reducer
export default createReducer(INITIAL_STATE, {
  [Types.POST_CASAMENTO_SUCCESS]: casamentoOk,
  [Types.POST_CASAMENTO_FAILURE]: casamentoFail,
  [Types.POST_CASAMENTO_REQUEST]: casamentoReq
})
