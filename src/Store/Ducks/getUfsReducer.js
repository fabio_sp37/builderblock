import { createActions, createReducer } from 'reduxsauce';

//Actions types and creators
export const { Types, Creators } = createActions({
  getUfsSuccess: ["list", "err", "loading"],
  getUfsFailure: ["list", "err", "loading"],
  getUfsRequest: ["loading"]
})

//Reducer handlers
const INITIAL_STATE = {
  err: false,
  loading: false,
  list: []
}

const ufsOk = (state = INITIAL_STATE, action) => (
  {
    list: action.list,
    err: false,
    loading: false
  }
)

const ufsFail = (state = INITIAL_STATE, action) => (
  {
    list: [],
    err: action.err,
    loading: false
  }
)

const ufsReq = (state = INITIAL_STATE, action) => (
  {
    list: [],
    err: false,
    loading: true
  }
)

//Reducer
export default createReducer(INITIAL_STATE, {
  [Types.GET_UFS_SUCCESS]: ufsOk,
  [Types.GET_UFS_FAILURE]: ufsFail,
  [Types.GET_UFS_REQUEST]: ufsReq
})
