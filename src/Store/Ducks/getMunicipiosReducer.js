import { createActions, createReducer } from 'reduxsauce';

//Actions types and creators
export const { Types, Creators } = createActions({
  getMunicipiosSuccess: ["list", "err", "loading"],
  getMunicipiosFailure: ["list", "err", "loading"],
  getMunicipiosRequest: ["loading"]
})

//Reducer handlers
const INITIAL_STATE = {
  err: false,
  loading: false,
  list: []
}

const municipiosOk = (state = INITIAL_STATE, action) => (
  {
    list: action.list,
    err: false,
    loading: false
  }
)

const municipiosFail = (state = INITIAL_STATE, action) => (
  {
    list: [],
    err: action.err,
    loading: false
  }
)

const municipiosReq = (state = INITIAL_STATE, action) => (
  {
    list: [],
    err: false,
    loading: true
  }
)

//Reducer
export default createReducer(INITIAL_STATE, {
  [Types.GET_MUNICIPIOS_SUCCESS]: municipiosOk,
  [Types.GET_MUNICIPIOS_FAILURE]: municipiosFail,
  [Types.GET_MUNICIPIOS_REQUEST]: municipiosReq
})
