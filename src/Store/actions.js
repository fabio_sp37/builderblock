export function postCasamento(data) {
  return {
    type: 'POST_CASAMENTO_REQUEST',
    data
  }
}

export function getUfs() {
  return {
    type: 'GET_UFS_REQUEST'
  }
}

export function getMunicipios(uf) {
  return {
    type: 'GET_MUNICIPIOS_REQUEST',
    uf
  }
}