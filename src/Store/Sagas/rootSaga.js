import { takeLatest, all } from 'redux-saga/effects'
import * as Sagas from './index'

export default function* root() {
  yield all([
    takeLatest('POST_CASAMENTO_REQUEST', Sagas.CasamentoSaga.casamentoCadastro),
    takeLatest('GET_UFS_REQUEST', Sagas.CasamentoSaga.getUfs),
    takeLatest('GET_MUNICIPIOS_REQUEST', Sagas.CasamentoSaga.getMunicipios)
  ])
}