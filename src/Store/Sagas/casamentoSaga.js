import { put, call } from 'redux-saga/effects'
import { getUfsFetch, getMunicipiosFetch } from '../services'

//Function Generations
function* casamentoCadastro(state) {
  try {
    const x = state.value;

    yield put(setCasamentoCadastroOk(x))
  }
  catch(err) {
    yield put(setCasamentoCadastroFail(err))
  }
}

function* getUfs() {
  try {
    const response = yield call(() => getUfsFetch())

    if(response.status !== 200) {
      throw response.status
    }

    const result = yield response.json()

    yield put(setUfsOk(result))
  }
  catch(err) {
    yield put(setUfsFail(err.message))
  }
}

function* getMunicipios(state) {
  try {
    const response = yield call(() => getMunicipiosFetch(state.uf))

    if(response.status !== 200) {
      throw response.status
    }

    const result = yield response.json()

    yield put(setMunicipiosOk(result))
  }
  catch(err) {
    yield put(setMunicipiosFail(err.message))
  }
}

export default { casamentoCadastro, getUfs, getMunicipios }

//Actions Setters
function setCasamentoCadastroOk(confirm) {
  return { type: 'CASAMENTO_POST_SUCCESS', confirm }
}

function setCasamentoCadastroFail(err) {
  return { type: 'CASAMENTO_POST_FAIL', err }
}

function setUfsOk(list) {
  return { type: 'GET_UFS_SUCCESS', list }
}

function setUfsFail() {
  return { type: 'GET_UFS_FAILURE' }
}

function setMunicipiosOk(list) {
  return { type: 'GET_MUNICIPIOS_SUCCESS', list }
}

function setMunicipiosFail() {
  return { type: 'GET_MUNICIPIOS_FAILURE' }
}