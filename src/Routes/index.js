import React from 'react'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import * as Views from '../Views/Index'

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route path="/(home|)/" component={Views.Gravatinha} />
        <Route path="/cadastro" component={Views.Home} />
        <Route path="/selecaocartorio" component={Views.Registry} />
        <Route path="/videowedding" component={Views.VideoWedding} />      
      </Switch>
    </Router>
  )
}

const RouteWithSubRoutes = (route) => {
  return (
      <Route
          path={route.path}
          render={props => 
              <route.component {...props} routes={route.routes} />
      } />
  )
}

export { Routes, RouteWithSubRoutes }