import React from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import LogoGoiaba from '../../Assets/images/logo-goiaba.svg'
import { FaRegUserCircle } from "react-icons/fa";
import { RiShoppingCartLine } from "react-icons/ri";
import '../../Assets/css/default.css'
import { Link } from 'react-router-dom';
import LogoMarie from '../../Assets/images/marie.png'

export const LjNavbar = () => {
  return (
    <>
      <Navbar collapseOnSelect bg-light expand="lg" className="__navBar">
        <Link to="/">
          <Navbar.Brand>
            <img src={LogoGoiaba} alt="Lejour Logo" />
          </Navbar.Brand>
        </Link>        
        <Navbar.Toggle aria-controls="lj-navbar-nav" />
        <Navbar.Collapse id="lj-navbar-nav">
        <Nav className="mr-auto">
        <img style={{marginLeft: "75vh"}} src={LogoMarie} alt="logo" width="100px" />
        </Nav>
        <Nav>
          <div className="d-inline-block align-top">
          <RiShoppingCartLine fontSize="32pt" className="mr-3" />
            <FaRegUserCircle fontSize="32pt" color="#86D0CB" />
          </div>
        </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  )
}