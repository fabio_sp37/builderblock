import React from 'react'
import { Row, Col, Container } from 'react-bootstrap'
import '../../Assets/css/default.css';
import { Link } from 'react-router-dom';
import Cartorio from '../../Assets/images/cartorio.PNG'

export const ChooseRegistry = () => {

  return (
      <div>
        <Container>
          <main className="mt-1">
            <section>
              <Row className="mt-5">
                <Col md="12" sm="12" xs="12">
                  <img src={Cartorio} alt="Cartorio" usemap="#image-map"/>
                  <map name="image-map">
                    <Link to="/videowedding">
                      <area target="" alt="" title="" coords="801,652,1279,592" shape="rect" />
                    </Link>
                  </map>
                </Col>
              </Row>
            </section>
          </main>
        </Container>
      </div>
  )
}