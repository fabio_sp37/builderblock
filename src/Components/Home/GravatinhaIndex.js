import React from 'react'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Row, Col, Container } from 'react-bootstrap'
import {
  makeStyles
} from '@material-ui/core/styles';
import Gravatinha from '../../Assets/images/Gravatinha.PNG'
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  textField: {
    marginLeft: 14,
    width: '90%'
  },
  formControl: {
    minWidth: 120,
    width: '780px'
  }
});

export const GravatinhaIndex = () => {
  const classes = useStyles()

  return (
    <div>
      <Container fluid>
          <main className="mt-1">
            <section>
              <Row className="mt-5">
                <Col md="12" sm="12" xs="12">
                  <article>
                    <Card className={classes.root}>
                      <CardContent className="d-flex justify-content-center">
                        <img width="1500px" height="auto" src={Gravatinha} alt="gravatinha" />
                      </CardContent>
                    </Card>
                  </article>  
                </Col>
              </Row>
              <Row className="mt-2">
                <Col md="4"></Col>
                <Col md="4">
                <Link to="/cadastro" 
                  className="MuiButtonBase-root MuiButton-root MuiButton-text"
                  buttonStyle={{ borderRadius: 25 }}
                  style={{ color: "#FFFFFF",width: "100%", height: "50px", borderRadius: 25, backgroundColor: "#81d8d0", boxShadow: "0 6px 20px -8px", padding: "3px 3px 3px 3px" }}
                >Cadastro de informações
                </Link>
                <Button
                  ></Button>
                </Col>
                <Col md="4"></Col>
              </Row>
            </section>
          </main>
      </Container>
    </div>
  )
}