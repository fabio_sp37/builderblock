import React, { useState, useEffect } from 'react'
import { Container, Col, Row, Image } from 'react-bootstrap'
import WeddingSvg from '../../Assets/images/wedding.svg'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import { useDispatch, useSelector } from 'react-redux'
import { getUfs, getMunicipios } from '../../Store/actions'
import {
  withStyles,
  makeStyles
} from '@material-ui/core/styles';
import '../../Assets/css/default.css'
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  textField: {
    width: '100%'
  },
  formControl: {
    minWidth: 120,
    width: '100%'
  }
});

const ValidationTextField = withStyles({
  root: {
    '& input:valid + fieldset': {
      borderWidth: 2,
    },
    '& input:invalid + fieldset': {
      borderColor: '#EA8079',
      borderWidth: 2,
    },
    '& input:invalid:focus + fieldset': {
      borderColor: '#84B8E2',
      padding: '4px !important',
    },
    '& input:valid:focus + fieldset': {
      borderColor: '#84B8E2',
      borderLeftWidth: 6,
      padding: '4px !important',
    },
  },
})(TextField);

export const HomeIndex = () => {
  const [state, setState] = useState({
    noivo: {
      show: false,
      showDivorciado: false,
      showViuvo: false
    },
    noiva: {
      show: false,
      showDivorciada: false,
      showViuva: false
    }
  })

  const noivoInitialState = {
    show: false,
    showDivorciado: false,
    showViuvo: false,
  }

  const noivaInitialState = {
    show: false,
    showDivorciada: false,
    showViuva: false
  }

  const classes = useStyles()

  const handleCivilState = (showing) => {
    if(showing.person === 1) {
      switch (parseInt(showing.val)) {
        case 1:
          setState(state.noivo = noivoInitialState)
          setState({ ...state, noivo: { ...state.noivo, show: true } })
          break
        case 2:
          setState(state.noivo = noivoInitialState)
          setState({ ...state, noivo: { ...state.noivo, showDivorciado: true } })
          break
        case 3:
          setState(state.noivo = noivoInitialState)
          setState({ ...state, noivo: { ...state.noivo, showViuvo: true } })
          break
        default:
          setState({ ...state, noivo: noivoInitialState })
          break
      }
    } else {
      switch (parseInt(showing.val)) {
        case 1:
          setState(state.noiva = noivaInitialState)
          setState({ ...state, noiva: { ...state.noiva, show: true } })
          break
        case 2:
          setState(state.noiva = noivaInitialState)
          setState({ ...state, noiva: { ...state.noiva, showDivorciada: true } })
          break
        case 3:
          setState(state.noiva = noivaInitialState)
          setState({ ...state, noiva: { ...state.noiva, showViuva: true } })
          break
        default:
          setState({ ...state, noiva: noivaInitialState })
          break
      }
    }
  }

  const dispatch = useDispatch()
  const ufs = useSelector(r => r.ufsReducer).list
  const municipios = useSelector(r => r.municipiosReducer).list

  useEffect(() => {
      dispatch(getUfs())
  }, [dispatch])

  const handleUF = (e) => {
    dispatch(getMunicipios(ufs.find(u => (u.sigla === e.target.value))?.id))
  }

  var curr = new Date();
  curr.setDate(curr.getDate() + 3);
  var date = curr.toISOString().substr(0,10);

  return (
    <div>
      <Container fluid>
        <main className="mt-5">
          <section>
            <Row>
              <Col md="6" sm="12" xs="12">
                <Image fluid className="img-responsive" src={WeddingSvg} alt="wedding" />
              </Col>
              <Col md="6" sm="12" xs="12">
                <header>
                  <Row>
                    <Col md="3" sm="3" xs="2"></Col>
                    <Col md="6" sm="6" xs="8">
                      <h5 className="titleLabels text-center mt-5">Aqui você oficializa seu matrimônio</h5>
                    </Col>
                    <Col md="3" sm="3" xs="2"></Col>
                  </Row>
                </header>
                <article>
                  <Card className={classes.root}>
                    <CardContent>
                      <Row>
                        <Col md="6" sm="12" xs="12">
                          <span className="subtitlesLabels d-flex justify-content-center">Cônjuge</span>
                            <Row>
                              <Col>
                                <FormControl variant="outlined" className={classes.formControl}>
                                  <InputLabel htmlFor="ec-noivo">Selecione</InputLabel>
                                  <Select
                                    native
                                    label="Selecione"
                                    inputProps={{
                                      name: 'selecione',
                                      id: 'ec-noivo',
                                    }}
                                    onChange={(e) => handleCivilState({ val: e.target.value, person: 1 })}
                                  >
                                    <option value={1}>Primeiro casamento</option>
                                    <option value={2}>Divorciado(a)</option>
                                    <option value={3}>Viúvo(a)</option>
                                  </Select>
                                </FormControl>
                              </Col>
                            </Row>
                            <Row className="mt-2">
                              <Col>
                                <ValidationTextField
                                  className={classes.textField}
                                  label="Nome completo de solteiro(a)"
                                  required
                                  variant="outlined"
                                  id="nmNoivo"
                                  defaultValue="Carlos"
                                />
                              </Col>
                            </Row>
                            <Row className="mt-3">
                              <Col>
                              <TextField
                                id="nascNoivo"
                                label="Data de nascimento"
                                type="date"
                                className={classes.textField}
                                InputLabelProps={{
                                  shrink: true,
                                }}
                                defaultValue={date}
                              />
                              </Col>
                            </Row>
                            <Row className="mt-3">
                              <Col>
                                <FormControl variant="outlined" className={classes.formControl}>
                                    <InputLabel htmlFor="ufNoivo"></InputLabel>
                                    <Select
                                      native
                                      label="UF de nascimento"
                                      inputProps={{
                                        name: 'uf',
                                        id: 'ufNoivo',
                                      }}
                                      defaultValue="12"
                                      onChange={handleUF}
                                      >
                                        <option value="">SP</option>
                                        {ufs.map(u => (
                                          <option key={u.id} value={u.sigla}>{u.sigla}</option>
                                        ))}
                                      </Select>
                                  </FormControl>
                              </Col>
                            </Row>
                            <Row className="mt-3">
                              <Col>
                                <FormControl variant="outlined" className={classes.formControl}>
                                    <InputLabel htmlFor="munNoivo"></InputLabel>
                                    <Select
                                      native
                                      label="Município de nascimento"
                                      inputProps={{
                                        name: 'municipio',
                                        id: 'munNoivo',
                                      }} >
                                        <option value="">Guarulhos</option>
                                        {municipios.map(m => (
                                          <option key={m.id} value={m.id}>{m.nome}</option>
                                        ))}
                                      </Select>
                                  </FormControl>
                              </Col>
                            </Row>
                            <Row className="mt-2">
                              <Col>
                                <ValidationTextField
                                  className={classes.textField}
                                  label="Nacionalidade"
                                  required
                                  variant="outlined"
                                  id="nacioNoivo"
                                  defaultValue="Brasileira"
                                />
                              </Col>
                            </Row>
                            <Row className="mt-2">
                              <Col>
                                <ValidationTextField
                                  className={classes.textField}
                                  label="Filiação 1"
                                  required
                                  variant="outlined"
                                  id="fil1Noivo"
                                  defaultValue="José"
                                />
                              </Col>
                            </Row>
                            <Row className="mt-2">
                              <Col>
                                <TextField
                                  className={classes.textField}
                                  label="Filiação 2"
                                  required
                                  variant="outlined"
                                  id="fil2Noivo"
                                  defaultValue="Maria"
                                />
                              </Col>
                            </Row>
                            <Row className="mt-2">
                              <Col>
                                <TextField
                                  className={classes.textField}
                                  label="Nome que passou a utilizar (caso alterado)"
                                  required
                                  variant="outlined"
                                  id="newNameNoivo"
                                />
                              </Col>
                            </Row>
                        </Col>
                        <Col md="6" sm="12" xs="12">
                          <span className="subtitlesLabels d-flex justify-content-center">Cônjuge</span>
                          <Row>
                              <Col>
                                <FormControl variant="outlined" className={classes.formControl}>
                                  <InputLabel htmlFor="ec-noiva">Selecione</InputLabel>
                                  <Select
                                    native
                                    label="Selecione"
                                    inputProps={{
                                      name: 'selecione',
                                      id: 'ec-noiva',
                                    }}
                                    onChange={(e) => handleCivilState({ val: e.target.value, person: 2 })}
                                  >
                                    <option value={1}>Primeiro casamento</option>
                                    <option value={2}>Divorciada(o)</option>
                                    <option value={3}>Viúva(o)</option>
                                  </Select>
                                </FormControl>
                              </Col>
                            </Row>
                            <Row className="mt-2">
                              <Col>
                                <ValidationTextField
                                  className={classes.textField}
                                  label="Nome completo de solteira(o)"
                                  required
                                  variant="outlined"
                                  id="nmNoiva"
                                  defaultValue="Henrique"
                                />
                              </Col>
                            </Row>
                            <Row className="mt-3">
                              <Col>
                              <TextField
                                id="nascNoiva"
                                label="Data de nascimento"
                                type="date"
                                className={classes.textField}
                                InputLabelProps={{
                                  shrink: true,
                                }}
                                defaultValue={date}
                              />
                              </Col>
                            </Row>
                            <Row className="mt-3">
                              <Col>
                                <FormControl variant="outlined" className={classes.formControl}>
                                    <InputLabel htmlFor="ufNoiva"></InputLabel>
                                    <Select
                                      native
                                      label="UF de nascimento"
                                      inputProps={{
                                        name: 'uf',
                                        id: 'ufNoiva',
                                      }}
                                      onChange={handleUF}
                                      >
                                        {ufs.map(u => (
                                          <option key={u.id} value={u.sigla}>{u.sigla}</option>
                                        ))}
                                      </Select>
                                  </FormControl>
                              </Col>
                            </Row>
                            <Row className="mt-3">
                              <Col>
                                <FormControl variant="outlined" className={classes.formControl}>
                                    <InputLabel htmlFor="munNoiva"></InputLabel>
                                    <Select
                                      native
                                      label="Município de nascimento"
                                      inputProps={{
                                        name: 'municipio',
                                        id: 'munNoiva',
                                      }} >
                                        <option value="">Jaru</option>
                                        {municipios.map(m => (
                                          <option key={m.id} value={m.id}>{m.nome}</option>
                                        ))}
                                      </Select>
                                  </FormControl>
                              </Col>
                            </Row>
                            <Row className="mt-2">
                              <Col>
                                <ValidationTextField
                                  className={classes.textField}
                                  label="Nacionalidade"
                                  required
                                  variant="outlined"
                                  id="nacioNoiva"
                                  defaultValue="Brasileira"
                                />
                              </Col>
                            </Row>
                            <Row className="mt-2">
                              <Col>
                                <ValidationTextField
                                  className={classes.textField}
                                  label="Filiação 1"
                                  required
                                  variant="outlined"
                                  id="fil1Noiva"
                                  defaultValue="Madalena"
                                />
                              </Col>
                            </Row>
                            <Row className="mt-2">
                              <Col>
                                <TextField
                                  className={classes.textField}
                                  label="Filiação 2"
                                  required
                                  variant="outlined"
                                  id="fil2Noiva"
                                  defaultValue="Pedro"
                                />
                              </Col>
                            </Row>
                            <Row className="mt-2">
                              <Col>
                                <TextField
                                  className={classes.textField}
                                  label="Nome que passou a utilizar (caso alterado)"
                                  required
                                  variant="outlined"
                                  id="newNameNoiva"
                                />
                              </Col>
                            </Row>
                        </Col>
                      </Row>
                      <Row className="mt-2">
                        <Col>
                          <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel htmlFor="regime">Regime de bens</InputLabel>
                            <Select
                              native
                              label="Regime de bens"
                              inputProps={{
                                name: 'regimebens',
                                id: 'regimaDeBens',
                              }} >
                                <option value="1">Comunhão Parcial</option>
                                <option value="2">Comunhão Universal</option>
                                <option value="3">Participação Final Nos Aquestos</option>
                                <option value="4">Separação Convencional de Bens</option>
                              </Select>
                          </FormControl>
                        </Col>
                      </Row>
                      <Row className="mt-2">
                        <Col>
                        <Link to="/selecaocartorio" 
                          className="MuiButtonBase-root MuiButton-root MuiButton-text"
                          buttonStyle={{ borderRadius: 25 }}
                          style={{ color: "#FFFFFF",width: "100%", height: "50px", borderRadius: 25, backgroundColor: "#81d8d0", boxShadow: "0 6px 20px -8px", padding: "3px 3px 3px 3px" }}
                        >Escolher cartório
                        </Link>
                        <Button
                          ></Button>
                        </Col>
                      </Row>
                    </CardContent>
                  </Card>
                </article>
              </Col>
            </Row>
          </section>
          <br />
        </main>
      </Container>
    </div>
  )
}
