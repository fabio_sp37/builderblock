import React from 'react'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Row, Col, Container } from 'react-bootstrap'
import {
  makeStyles
} from '@material-ui/core/styles';
import { FaRegHeart } from "react-icons/fa";
import Conf from '../../Assets/images/vai.001.jpeg'

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
  textField: {
    marginLeft: 14,
    width: '90%'
  },
  formControl: {
    minWidth: 120,
    width: '780px'
  }
});

export const VideoConf = () => {
  const classes = useStyles()

  return (
    <div>
      <Container fluid>
          <main className="mt-1">
            <section>
              <Row className="mt-5">
                <Col md="12" sm="12" xs="12">
                  <header>
                    <Row>
                      <Col md="3" sm="3" xs="2"></Col>
                      <Col md="6" sm="6" xs="8">
                        <h5 className="titleLabels text-center mt-5">Carlos <FaRegHeart style={{ color: "#DB5D79" }} /> Henrique</h5>
                      </Col>
                      <Col md="3" sm="3" xs="2"></Col>
                    </Row>
                  </header>
                  <article>
                    <Card className={classes.root}>
                      <CardContent className="d-flex justify-content-center">
                        <img width="1500px" height="auto" src={Conf} alt="Conf" />
                      </CardContent>
                    </Card>
                  </article>  
                </Col>
              </Row>
            </section>
          </main>
      </Container>
    </div>
  )
}