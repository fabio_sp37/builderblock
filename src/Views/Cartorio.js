import React from 'react'
import { ChooseRegistry } from '../Components/Home/chooseRegistry'
import { LjNavbar } from '../Components/Default/LjNavbar'

const Registry = () => {
  return (
    <>
      <LjNavbar />
      <ChooseRegistry />
    </>
  )
}

export default Registry