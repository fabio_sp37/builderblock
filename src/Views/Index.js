export {default as Home} from './Home'
export {default as Registry} from './Cartorio'
export {default as VideoWedding} from './VideoWedding'
export {default as Gravatinha} from './Gravatinha'