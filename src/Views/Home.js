import React from 'react'
import { HomeIndex } from '../Components/Home/HomeIndex'
import { LjNavbar } from '../Components/Default/LjNavbar'

const Home = () => {
  return (
    <>
      <LjNavbar />
      <HomeIndex />
    </>
  )
}

export default Home