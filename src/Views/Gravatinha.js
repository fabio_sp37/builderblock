import React from 'react'
import { GravatinhaIndex } from '../Components/Home/GravatinhaIndex'
import { LjNavbar } from '../Components/Default/LjNavbar'

const Gravatinha = () => {
  return (
    <div>
      <LjNavbar />
      <GravatinhaIndex />
    </div>
  )
}

export default Gravatinha