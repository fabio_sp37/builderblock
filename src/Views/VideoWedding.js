import React from 'react'
import { VideoConf } from '../Components/Home/videoConf'
import { LjNavbar } from '../Components/Default/LjNavbar'

const VideoWedding = () => {
  return (
    <div>
      <LjNavbar />
      <VideoConf />
    </div>
  )
}

export default VideoWedding